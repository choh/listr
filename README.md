# The `listr` package
## Overview
The `listr` package aims to make your life a little bit easier when working 
with lists.
Lists are common when working with R, most notably you will find them as 
output from functions such as `base::lapply` or `purrr::map`, but they are
also the only proper way to return multiple values from a function.

While a lot of tools are now available making work with rectangular data a 
breeze, lists have not seen that much love. 
The idea behind the `listr` package is to close that gap at least little bit.

## Installation
The package is on CRAN, so you can install the latest stable version 
using

```
install.packages("listr")
```

When new releases happen the repository here might be slightly ahead of 
what you can download from CRAN.
You find the latest features and fixes in the non-`main` branches. 
