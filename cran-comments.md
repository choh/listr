This is the version 0.1 release of the 'listr' package.

## R CMD check results
Locally there were no NOTEs, WARNINGs and ERRORs.

Using the R-Hub service there was one note on Windows Server 2022, R-devel, 
64 bit only:

* checking for detritus in the temp directory
    Found the following files/directories:
      'lastMiKTeXException'

On trying to diagnose what causes this, it appears to be a common NOTE
with R-Hub Windows.

Using the R-Hub service there was one note on Fedora Linux, R-devel, only:

* checking HTML version of manual ... NOTE Skipping checking HTML validation: no command 'tidy' found

I believe this is an issue with the remote system and not with the package.
